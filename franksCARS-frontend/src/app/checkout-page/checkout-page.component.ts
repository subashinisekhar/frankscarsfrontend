import {Component, OnInit} from '@angular/core';
import {UtilService} from '../services/util.service';
import {Car} from '../Car';
import {BehaviorSubject, Subscription} from 'rxjs';


@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.css']
})
export class CheckoutPageComponent implements OnInit {

  constructor(private utilityService: UtilService) {
  }

  carsInCart: Car[];
  carSub: Subscription;
  totalCost$ = new BehaviorSubject<number>(0);


  ngOnInit() {
    this.carsInCart = this.utilityService.carsInCart;
    this.calculateTotalCost();
    this.carSub = this.utilityService.carRemoved.subscribe(carsInCart => {
      this.carsInCart = carsInCart;
      this.calculateTotalCost();
    });
  }

  removeCar(car) {
    this.utilityService.carsInCart = this.utilityService.carsInCart.filter(c => c.id !== car.id);
    this.utilityService.carRemoved.emit(this.utilityService.carsInCart);
    this.utilityService.carsinCartLength$.next(this.utilityService.carsInCart.length);
  }

  calculateTotalCost() {
    let total = 0;
    this.utilityService.carsInCart.forEach(car => {
      total = total + car.price;
      this.totalCost$.next(total);
    });
  }

}
