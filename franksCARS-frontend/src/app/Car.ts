export interface Car {
  id: number;
  make: string;
  model: string;
  licensed: boolean;
  date_added: Date;
  img_url: string;
  kms: number;
  price: number;
  co2: number;
  fuel_type: string;
  warehouse: string;
}
