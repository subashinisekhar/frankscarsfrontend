import {RouterModule, Routes} from '@angular/router';
import {HomepageComponent} from './homepage/homepage.component';
import {NgModule} from '@angular/core';
import {CarDetailsComponent} from './car-details/car-details.component';
import {CheckoutPageComponent} from './checkout-page/checkout-page.component';
import {ErrorPageComponent} from './error-page/error-page.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'car/:id', component: CarDetailsComponent},
  {path: 'checkout', component: CheckoutPageComponent},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'}},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule {
}
