import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UtilService} from '../services/util.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private router: Router, public utilityService: UtilService) {
  }

  ngOnInit() {}

  checkout() {
    this.router.navigate(['checkout']);
  }
}
