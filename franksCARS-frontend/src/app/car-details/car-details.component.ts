import {Component, OnInit} from '@angular/core';
import {UtilService} from '../services/util.service';
import {Car} from '../Car';

import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private utilityService: UtilService) {
  }

  private carid: number;
  public car$: Observable<Car>;

  ngOnInit() {
    if (this.route.snapshot.params['id']) {
      this.carid = +this.route.snapshot.params['id'];
      this.car$ = this.utilityService.getCarDetails(this.carid);
    }
  }

  addToCart(car: Car) {
    this.utilityService.carsInCart.push(car);
    this.utilityService.carAdded.emit(this.utilityService.carsInCart.length);
    this.utilityService.carsinCartLength$.next(this.utilityService.carsInCart.length);
  }
}
