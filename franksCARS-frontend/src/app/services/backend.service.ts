import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

import {Car} from '../Car';
import {Observable} from 'rxjs';

@Injectable()
export class BackendService {

  private allcars_endpoint = environment.api_url + 'api/cars';

  constructor(private http: HttpClient) {
  }

  public getAllCars(): Observable<Car[]> {
    return this.http.get<Car[]>(this.allcars_endpoint);
  }


}
