import {EventEmitter, Injectable, Output} from '@angular/core';
import {Car} from '../Car';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class UtilService {
  allCars$: Observable<Car[]>;
  carsInCart: Car[] = [];
  carsinCartLength$ = new BehaviorSubject<number>(0);

  @Output() carAdded = new EventEmitter();
  @Output() carRemoved = new EventEmitter();

  constructor() {
  }

  getCarDetails(carid: number): Observable<Car> {
    return this.allCars$.pipe(
      map(cars => cars.find(car => car.id === carid))
    );
  }
}
