import {Component, OnInit} from '@angular/core';
import {BackendService} from '../services/backend.service';

import {Car} from '../Car';
import {map} from 'rxjs/operators';
import {UtilService} from '../services/util.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css']
})
export class CarsListComponent implements OnInit {

  allCars$: Observable<Car[]>;
  allCarsDatesAddedASC$: Observable<Car[]>;

  constructor(private backendService: BackendService, private utilService: UtilService) {
  }

  ngOnInit() {
    this.allCars$ = this.backendService.getAllCars();
    this.utilService.allCars$ = this.allCars$;
    this.sortCarsByAsc();
  }

  sortCarsByAsc(): Observable<Car[]> {
    this.allCarsDatesAddedASC$ = this.allCars$.pipe(
      map(cars => cars.sort((a, b) => new Date(a.date_added).getDate() - new Date(b.date_added).getDate()))
    );
    return this.allCarsDatesAddedASC$;

  }
}
