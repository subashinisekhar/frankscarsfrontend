import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {UtilService} from './services/util.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CheckoutPageComponent } from './checkout-page/checkout-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import {BackendService} from './services/backend.service';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    CarDetailsComponent,
    CarsListComponent,
    CheckoutPageComponent,
    ErrorPageComponent,
    ShoppingCartComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [BackendService, UtilService],
  bootstrap: [AppComponent]
})
export class AppModule { }
